// Exponents
/* Math.pow() to get results of a number raised to a given exponent
Math.pow(base,exponent)*/ 

// let fivePowerOf3 = Math.pow(5,3);
// console.log(fivePowerOf3);

/* Exponent Operators ** = allow us to get the result 
of a number raised to a given exponent */

let fivePowerOf3 = 5**3;
console.log(fivePowerOf3);

// Mini Activity
let fivePowerOf2 = 5**2;
let fivePowerOf4 = 5**4;
let twoPowerOf2 = 2**2;
console.log(fivePowerOf2);
console.log(fivePowerOf4);
console.log(twoPowerOf2);


function squareRootChecker(number){
	return number**.5;
}

let squareRootOf4 = squareRootChecker(4);
console.log(squareRootOf4);

// Template Literal

/* An Es6 Update to creating strings. Represented by backtick (``).
	'',"" - string literals
	`` - backticks/template literals
*/

let sampleString1 = `Charlie`;
let sampleString2 = `Joy`;
console.log(sampleString1);
console.log(sampleString2);

let combinedString = `${sampleString1} ${sampleString2}`;
console.log(combinedString);

/* We can create a single string to combine our variables. 
with the use of template literals(``) and by embedding JS 
expressions and variables in the string using ${} or placeholders
*/
let num1 = 15;
let num2 = 3;
let sentence = `The result of ${num1} plus ${num2} is ${num1+num2}`;
console.log(sentence);

//Mini Activity
let sentence2 = `The result of 15 to the power of 2 is ${15**2}`;
let sentence3 = `The result of 3 times 6 is ${3*6}`;

console.log(sentence2);
console.log(sentence3);

// Array Destructuring
/* when we destructure an array, we save array items in variables */
let array = ["Kobe", "Lebron", "Jordan"];
/*
let goat1 = array[0];
console.log(goat1);

let goat2 = array[1];
let goat3 = array[2];
console.log(goat2,goat3);
*/

let [goat1, goat2, goat3] = array;
console.log(goat1);
console.log(goat2);
console.log(goat3);

let array2 = ["Curry","Lillard","Paul","Irving"];
let [pg1,pg2,pg3,pg4] = array2;
console.log(pg1);
console.log(pg2);
console.log(pg3);
console.log(pg4);
console.log(array2);


let pokemon = {
	name: "Blastoise",
	level: 40,
	health: 80
}

// Mini-Activity:

let sentence4 = `The pokemon's name is ${pokemon.name}`
let sentence5 = `It is a level ${pokemon.level} pokemon.`
let sentence6 = `It has at least ${pokemon.health} health points.`

console.log(sentence4);
console.log(sentence5);
console.log(sentence6);

// We can save values of an object's properties into variables
// By Object Destructuring:

/* Array Destructuring, order was important and the name of the
variable we save our items in is not important.

Object destructuring, order is not important however the name of the variables
should match the properties of the object. Else, may result to undefined*/

let {health,name,level,trainer} = pokemon;
console.log(health);
console.log(name);
console.log(level);
console.log(trainer);

let person = {
	name: "Paul Phoenix",
	age: 31,
	birthday: "January 12, 1991"
}

// Mini Activity
// function greet({name,age,birthday}) {

// 	// destructure your object in the function
// 	console.log(`Hi! My name is ${name}.`);
// 	console.log(`I am ${age} years old.`);
// 	console.log(`My birthday is on ${birthday}.`);
// }
// greet(person);

// Mini Activity
let actors = ["Tom Hanks", "Leonardo DiCarpio", "Anthony Hopkins","Ryan Reynolds"];
let [actor1,actor2,actor3] = actors;

let director = {
	name: "Alfred Hitchcock",
	birthday: "August 13, 1889",
	isActive: false,
	movies: ["The Birds", "Psycho", "North by Northwest"]
};

function displayDirector({name,birthday,movies}){
	
	console.log(`The Director's name is ${name}`);
	console.log(`He was born on ${birthday}`);
	console.log(`His Movies include:`);
	console.log(movies);
}

console.log(actor1);
console.log(actor2);
console.log(actor3);
displayDirector(director);

/* Arrow Functions - alternative of writing/declaring functions. However,
there are significant difference between our regular/traditional function
and arrow functions.*/

// regular/traditional function
function displayMsg(){
	console.log(`Hello, World!`)
}
displayMsg();

// arrow functions
const hello = () => {
	console.log(`Hello from Arrow!`)
}

hello();

// function greet(personParams){
// 	console.log(`Hi, ${personParams.name}!}`)
// }

// Mini Activity

const greet = (personParams) => {
	console.log(`Hi, ${personParams.name}!`)
}
greet(person);

// anonymous functions in array methods
/* allows us to iterate/loop over all items in an array
-so we can perform tasks per item in array
-receives current item being iterated/looped

	console.log(actor);

	traditional functions need {}, 
	arrow functions can BUT it has to be a one-liner
*/
actors.forEach(actor=>console.log(actor));

/* .map() is similar to forEach wherein we can iterate over all items in our array
difference is we can return items from a map and create a new array out of it. */

// Mini Activity
let numberArr = [1,2,3,4,5];
// let multipliedBy5 = numberArr.map(function(number){
// 	return number*5;
// })

let multipliedby5 = numberArr.map(number=>{return number*5});
/* arrow functions do not need return keyword to return a value.
this is called implicit return. 
when arrow function have curly brace, it will need to have a 
RETURN keyword to return a value. */
let multipliedBy5 = numberArr.map(number=>number*5);

console.log(multipliedBy5);

// Implicit Return for Arrow Functions
// function addNum(num1,num2){
// 	return (num1+num2);
// };

// let sum1 = addNum(5,10);
// console.log(sum1);

const subtractNum = (num1,num2) => num1 - num2;
let difference1 = subtractNum(45,15);
console.log(difference1);
/* Even without a return keyword, arrow functions can return a value
So long as its code block is not wrapped with {} */

// Mini Activity
// updated addNum()
const addNum = (num1,num2)=>num1 + num2;
let sumExample = addNum(50,10);
console.log(sumExample);

//this keyword in a method
let protagonist = {
	name: "Cloud Strife",
	occupation: "SOLDIER",
	greet: function(){
		console.log(this);
		/* this refers to obj where method is in 
		for methods created as traditional functions */
		console.log(`Hi! I am ${this.name}`);
	},
	introduceJob: () => {
		/* this keyword in an arrow function does not refer to
		parent obj but refers to global window obj or the whole page
		*/
		console.log(this);
		console.log(`I work as a ${this.occupation}`);
	}
}

protagonist.greet();
protagonist.introduceJob();

/* Class-Based Objects Blueprints
In JS, classes are templates/blueprints to creating objects
We can create objects out of the use of Classes.
Before intro of Classes in JS, we mimic this behavior
to create objects out of templates with the use of 
constructor functions */

// Constructor function
function Pokemon(name,type,level){
	this.name = name;
	this.type = type;
	this.level = level;
}

let pokemon1 = new Pokemon("Sandshrew", "Ground",15);
console.log(pokemon1);

// Es6 Class Creation
/* We have a more distinctive way of creating classes
instead of just our constructor functions
*/

class Car {
constructor(brand,name,year){
	this.brand = brand;
	this.name = name;
	this.year = year;
}
}

let car1 = new Car("Toyota", "Vios", "2002");
console.log(car1);











